VERSION=`git describe | sed 's/^v//; s/-/./g' `
NAME="cut-publik-theme"

prefix = /usr

all: css

css:
	touch publik-base-theme/static/includes/_data_uris.scss
	cd static/grandlyon-cut/ && sassc --sourcemap style.scss style.css
	rm -rf static/*/.sass-cache/

clean:
	rm -rf sdist publik-base-theme/static/includes/_data_uris.scss

DIST_FILES = \
	Makefile static\
	templates themes.json \
	publik-base-theme

dist: clean
	-mkdir sdist
	rm -rf sdist/$(NAME)-$(VERSION)
	mkdir -p sdist/$(NAME)-$(VERSION)
	for i in $(DIST_FILES); do \
		cp -R "$$i" sdist/$(NAME)-$(VERSION); \
	done

install:
	mkdir -p $(DESTDIR)$(prefix)/share/publik/themes/grandlyon-cut
	cp -r static templates themes.json $(DESTDIR)$(prefix)/share/publik/themes/grandlyon-cut

dist-bzip2: dist
	-mkdir sdist
	cd sdist && tar cfj ../sdist/$(NAME)-$(VERSION).tar.bz2 $(NAME)-$(VERSION)

version:
	@(echo $(VERSION))

name:
	@(echo $(NAME))

fullname:
	@(echo $(NAME)-$(VERSION))
